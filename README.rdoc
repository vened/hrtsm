== README

Проект написан на ruby on rails

для запуска надо чтоб на локальной машине был установлен ruby

в корне проекта делаем bundle install

деплоится с помощью капистрано

команда для деплоя - cap production deploy

запуск сервера  - cap production puma:start
перезапуск сервера  - cap production puma:restart
остановка сервера  - cap production puma:stop